package com.analiticalprogram;

import com.analiticalprogram.resources.resource.AnaliticalResource;
import com.analiticalprogram.resources.resource.AnaliticalService;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class AnaliticalProgramApplication extends Application<AnaliticalProgramConfiguration> {

    public static void main(final String[] args) throws Exception {
        new AnaliticalProgramApplication().run(args);
    }

    /*private final HibernateBundle<AnaliticalProgramConfiguration> hibernate = new
    HibernateBundle<AnaliticalProgramConfiguration>(null)
    {
        @Override
        public DataSourceFactory getDataSourceFactory(AnaliticalProgramConfiguration configuration) {

            return configuration.getDataSourceFactory();
        }
    };*/

    @Override
    public String getName() {
        return "Analitical Program";
    }

    @Override
    public void initialize(final Bootstrap<AnaliticalProgramConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final AnaliticalProgramConfiguration configuration,
                    final Environment environment) {
        // TODO: implement application
        AnaliticalService service=new AnaliticalService();
        environment.jersey().register(new AnaliticalResource(service));
    }

}
