package com.analiticalprogram.resources.resource;

import com.analiticalprogram.api.AnaliticalProgram;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Luana Chavez on 07/11/2016.
 */
@Path("AnaliticalProgram")
@Produces(MediaType.APPLICATION_JSON)
public class AnaliticalResource {
        private final AnaliticalService service;

    public AnaliticalResource(AnaliticalService service) {
        this.service = service;
    }

    @GET
        public AnaliticalProgram getAnalitical()
        {
            try
            {
                return service.getAnaliticalProgram();
            }
            catch(Exception e)
            {
                return  null;
            }
        }
}
