package com.analiticalprogram.api;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Luana Chavez on 07/11/2016.
 */
public class AnaliticalProgram {
    private long id;
    private String subject;
    private int hourTheory;
    private int hourPractice;
    private int credit;
    private String subjectSoport;
    private String objective;

    public AnaliticalProgram() {
    }

    public AnaliticalProgram(long id, String subject, int hourTheory, int hourPractice, int credit, String subjectSoport, String objective) {
        this.id = id;
        this.subject = subject;
        this.hourTheory = hourTheory;
        this.hourPractice = hourPractice;
        this.credit = credit;
        this.subjectSoport = subjectSoport;
        this.objective = objective;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty
    public String getSubject() {
        return subject;
    }

    @JsonProperty
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @JsonProperty
    public int getHourTheory() {
        return hourTheory;
    }

    @JsonProperty
    public void setHourTheory(int hourTheory) {
        this.hourTheory = hourTheory;
    }

    @JsonProperty
    public int getHourPractice() {
        return hourPractice;
    }

    @JsonProperty
    public void setHourPractice(int hourPractice) {
        this.hourPractice = hourPractice;
    }

    @JsonProperty
    public int getCredit() {
        return credit;
    }

    @JsonProperty
    public void setCredit(int credit) {
        this.credit = credit;
    }

    @JsonProperty
    public String getSubjectSoport() {
        return subjectSoport;
    }

    @JsonProperty
    public void setSubjectSoport(String subjectSoport) {
        this.subjectSoport = subjectSoport;
    }

    @JsonProperty
    public String getObjective() {
        return objective;
    }

    @JsonProperty
    public void setObjective(String objective) {
        this.objective = objective;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AnaliticalProgram that = (AnaliticalProgram) o;

        if (id != that.id) return false;
        if (hourTheory != that.hourTheory) return false;
        if (hourPractice != that.hourPractice) return false;
        if (credit != that.credit) return false;
        if (subject != null ? !subject.equals(that.subject) : that.subject != null) return false;
        if (subjectSoport != null ? !subjectSoport.equals(that.subjectSoport) : that.subjectSoport != null)
            return false;
        return objective != null ? objective.equals(that.objective) : that.objective == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + hourTheory;
        result = 31 * result + hourPractice;
        result = 31 * result + credit;
        result = 31 * result + (subjectSoport != null ? subjectSoport.hashCode() : 0);
        result = 31 * result + (objective != null ? objective.hashCode() : 0);
        return result;
    }

    @Override
    public java.lang.String toString() {
        return "AnaliticalProgram{" +
                "id=" + id +
                ", subject=" + subject +
                ", hourTheory=" + hourTheory +
                ", hourPractice=" + hourPractice +
                ", Credit=" + credit +
                ", subjectSoport=" + subjectSoport +
                ", objective=" + objective +
                '}';
    }
}
