package com.analiticalprogram.db.AnaliticalProgram;

import com.analiticalprogram.api.AnaliticalProgram;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * Created by dbatista on 08-11-16.
 */
public class AnaliticalProgramRealDAO extends AbstractDAO<AnaliticalProgram> implements AnaliticalProgramDAO {

    public AnaliticalProgramRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public AnaliticalProgram findAnaliticalProgramById(long id) {
        return null;
    }

    @Override
    public long create(AnaliticalProgram analiticalProgram) {
        return persist(analiticalProgram).getId();
    }
}
