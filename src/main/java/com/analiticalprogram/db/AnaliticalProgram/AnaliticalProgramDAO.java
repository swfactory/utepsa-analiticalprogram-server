package com.analiticalprogram.db.AnaliticalProgram;

import com.analiticalprogram.api.AnaliticalProgram;

/**
 * Created by dbatista on 08-11-16.
 */
public interface AnaliticalProgramDAO {
    AnaliticalProgram findAnaliticalProgramById(long id);
    long create(AnaliticalProgram analiticalProgram);
}
