package com.analiticalprogram.db.AnaliticalProgram;

import com.analiticalprogram.api.AnaliticalProgram;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dbatista on 08-11-16.
 */
public class AnaliticalProgramFakeDAO implements AnaliticalProgramDAO {
    final private Map<Long,AnaliticalProgram> AnaliticalProgramTable=new HashMap<>();

    @Override
    public AnaliticalProgram findAnaliticalProgramById(long id) {
        for(Map.Entry<Long,AnaliticalProgram>AnaliticalProgram : this.AnaliticalProgramTable.entrySet())
        {
            if(AnaliticalProgram.getValue().getId()==id)
            {
                return this.AnaliticalProgramTable.get(AnaliticalProgram.getValue().getId());
            }
        }
        return null;
    }

    @Override
    public long create(AnaliticalProgram analiticalProgram) {
        AnaliticalProgramTable.put(analiticalProgram.getId(),analiticalProgram);
        return analiticalProgram.getId();
    }
}
